#include "Piece.h"
#include "Pawn.h"
#include <iostream>


using std::string;
using std::pair;

//determines legal move shape for pawn
bool Pawn::legal_move_shape(pair <char, char> start,
			    pair <char, char> end) const {

  //add to README: different legal move shapes for white and black
  
  if (is_white()) {//can only move up
    if (start.second == '2') {//allows to move two spaces
      return start.first==end.first && (end.second - start.second == 1 || end.second - start.second == 2);
    }
    else {//only move one space
      return start.first == end.first && end.second - start.second == 1;
    }
  }
  else {//can only move down
    if (start.second == '7') {//allows to move two spaces
      return start.first == end.first && (start.second - end.second == 1 || start.second - end.second == 2); 
    }
    else {//can only move one
      return start.first = end.first && start.second - end.second == 1;
    }
  }
}

//determines legal capture shape for pawn
bool Pawn:: legal_capture_shape(pair <char, char> start, pair <char, char> end) const {

  if (is_white()) { //check if up diagonal by 1
    return end.second - start.second == 1 && (start.first - end.first == 1 ||
					      start.first - end.first == -1);
  }

  else { //check if down diagonal by 1
    return start.second - end.second == 1 && (start.first - end.first == 1 ||
					      start.first - end.first == -1);

  }


}
