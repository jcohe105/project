//knight.cpp

#include "Knight.h"

bool Knight::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //All if statements must be true for move to be valid
  if (end.first == (start.first + 1) || end.first == (start.first - 1)) {//if end is one col left or right
    if (end.second == (start.second + 2) || end.second == (start.second - 2)) {//then the row must be 2 up or down
      return true;
    }
  }  
  if (end.second == (start.second + 1) || end.second == (start.second - 1)) {//if end is one row up or down
    if (end.first == (start.first + 2) || end.first == (start.first - 2)) {//then the col must be 2 left or right
      return true;
    }
  }
  
  return false;//not legal if not in one of those 8 location around start
}
