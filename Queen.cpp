//queen.cpp
#include <cmath> //Sarah said it's okay to use the C library in this case
#include "Queen.h"


bool Queen::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {

  bool lettersChange = start.first != end.first;
  bool numbersChange = start.second != end.second;
  if (lettersChange && !numbersChange) {
    //piece is ONLY moving along letters
    return true;
  }
  else if (!lettersChange && numbersChange) {
    //piece is ONLY moving along numbers
    return true;
  }
  else if (lettersChange && numbersChange) {
    //piece is moving on some diagonal
    //check if it's a 45deg diagonal
    int deltaX = abs(start.first - end.first);
    int deltaY = abs(start.second - end.second);
    return deltaX == deltaY;
  }
  else {
    //piece hasn't moved
    return false;
  }
}
