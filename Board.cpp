#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"

using std::cout;
using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}


Board::~Board( void ) {
  //deep destroy
  for(std::map<std::pair<char, char>, Piece*>::const_iterator it = _occ.cbegin(); it != _occ.cend(); ++it) {
    delete it->second;
  }
}

const Piece* Board::operator()( std::pair< char , char > position ) const
{
   // Returns a const pointer to the piece at a prescribed location if it exists, or a NULL pointer if there is nothing there.
  if (_occ.find(position) != _occ.end()) {
    return _occ.at(position);
  }
  //piece is not on map
  return NULL;
}

Board& Board::operator=(const Board b) {
  for(std::map<std::pair<char, char>, Piece*>::const_iterator it = b._occ.cbegin(); it != b._occ.cend(); ++it) {
    //make copy of piece at the old board's location it
    Piece *p = create_piece(it->second->to_ascii());
    _occ[it->first] = p; //stores in new board's _occ
  }
  return *this;
}

Board::Board(const Board &b) {
  for(std::map<std::pair<char, char>, Piece*>::const_iterator it = b._occ.cbegin(); it != b._occ.cend(); ++it) {
    //make copy of piece at the old board's location it
    Piece *p = create_piece(it->second->to_ascii());
    _occ[it->first] = p; //stores in new board's _occ
  }
}

//adds piece to board
bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{
  if (_occ[position] != NULL) { //can only add piece to empty spot
    return false;
  }
  _occ[ position ] = create_piece( piece_designator ); //add new piece
  return true;
}

//removes piece from board
bool Board::remove_piece(std::pair< char , char > position) {
  if (_occ.find(position) != _occ.end()) {
    delete _occ[position];//if this is not here memory is leaked
    _occ.erase(position);
    return true;//piece is removed
  }
  return false;
}

//check if there is 1 king of each color on the board
bool Board::has_valid_kings( void ) const
{
  std::pair <char, char> spot;
  int num_black = 0;
  int num_white = 0;
  for (char r = '1'; r <= '8'; r++) {//loops through every spot on the board
    for (char c = 'A'; c <= 'H'; c++) {//to check if that spot holds a king
      spot = std::make_pair(c, r);//spot on board
      if (_occ.find(spot) != _occ.end()) {
	const Piece* piece = _occ.at(spot);
	if (piece && piece->to_ascii() == 'k') {
	  num_black++;
	}
	else if (piece && piece->to_ascii() == 'K') {//white king
	  num_white++;
	}
      }
    }
   }
  return num_black == 1 && num_white == 1;//must be 2 - one of each
}

void Board::display( void ) const//prints current state of board
{
  Terminal::color_fg(false, Terminal::MAGENTA);
  cout << "ABCDEFGH" << endl;//we used the letter on top to help read
  Terminal::color_fg(false, Terminal::GREEN);
  cout << *this;;//the board
  Terminal::color_fg(false, Terminal::MAGENTA);
  cout << "ABCDEFGH" << endl;//we used the letter on bottom to help read
  Terminal::color_fg(false, Terminal::BLUE);
  cout << "Top row is 8, bottom row is 1" << endl;
  cout << endl;
  Terminal::set_default();
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}
