#include "Rook.h"
#include "Board.h"


//check if legal move shapre for rook
bool Rook::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  bool nonDiagonalMove;
  if (start.first != end.first) {
    //piece is moving along the letters
    nonDiagonalMove = start.second == end.second;
  }
  else if (start.second != end.second) {
    //piece is moving along the numbers
    nonDiagonalMove = start.first == end.first;
  }
  int inNewLoc = start != end;//has to move to new location
  return (nonDiagonalMove && inNewLoc);
}
