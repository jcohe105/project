#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"

class Rook : public Piece
{
public:
        /** This function checks if a rook object is allowed to move from a starting location to an end location legally
	    @param start the start location of the rook
	    @param end the end location of the rook
	    @returns true if the move is legal, false if the move is illegal.
	**/
        bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const;

	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii( void ) const { return is_white() ? 'R' : 'r'; }

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Rook( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // ROOK_H
