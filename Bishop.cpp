#include <string>
#include "Piece.h"
#include "Bishop.h"                                                                                               

using std::string;
//determines if legal move shape for a bishop
bool Bishop::legal_move_shape( std::pair< char , char > start,
			    std::pair< char , char > end) const {
  //logic: the absolute value of change in row = absolute value of change in column
  if (end.first - start.first == end.second - start.second ||
      end.first - start.first == start.second - end.second) {
    return true;
  }

  return false;
}
