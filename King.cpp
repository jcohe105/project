#include <string>
#include <cmath>
#include "King.h"

using std::string;
using std::abs;

//logic: the change in row = 0 or abs(1) and the change in column = 0 or abs(1)
bool King::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  if (abs(start.first-end.first) <= 1 && abs(start.second-end.second) <= 1) {//valid move for a King                                  
    return true;
  }
  return false;
}
