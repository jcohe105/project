#ifndef QUEEN_H
#define QUEEN_H

#include "Piece.h"

class Queen : public Piece
{
public:
        /** This function checks if a queen object is allowed to move
	    from a starting location to an end location legally
	    @param start the start location of the queen
	    @param end the end location of the queen
	    @returns true if the move is legal, false if the move is illegal.
	**/
        bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const;
  
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii( void ) const { return is_white() ? 'Q' : 'q'; }

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Queen( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // ROOK_H
