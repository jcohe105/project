#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess
{
public:
	// This default constructor initializes a board with the standard piece positions, and sets the state to white's turn
	Chess( void );

	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& board( void ) const { return _board; }

	// Returns true if it's white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white( void ) const { return _turn_white; }

	void change_turn(bool white) {_turn_white = white;}

	//Checks if a potential move is valid
	bool test_move(std:: pair<char, char > start, std:: pair<char, char > end) const;

	// Attempts to make a move. If successful, the move is made and the turn is switched white <-> black
	bool make_move( std::pair< char , char > start , std::pair< char , char > end );

	//checks whether path is clear if the path is a straight line
	bool clear_path (std::pair <char, char > start , std::pair< char , char > end) const;

	  //Checks whether a player's potential move will put the player in check
	bool self_check (std::pair< char , char > start , std::pair< char , char > end);
	
	//returns location of king
	std::pair< char, char> find_king ( bool white ) const;

	// Returns true if the designated player is in check
	bool in_check( bool white ) const;

	// Returns true if the designated player is in mate
	bool in_mate( bool white ) const;

	// Returns true if the designated player is in mate
	bool in_stalemate( bool white ) const;

	// Removes all pieces from _board
	void empty_board( void );

	friend std::istream& operator >> ( std::istream& is , Chess& chess );
private:

	// The board
	Board _board;

	// Is it white's turn?
	bool _turn_white;
};

// Writes the board out to a stream
std::ostream& operator << ( std::ostream& os , const Chess& chess );

// Reads the board in from a stream
std::istream& operator >> ( std::istream& is , Chess& chess );


#endif // CHESS_H
