#include "Chess.h"
#include "Board.h"
#include <iostream>
#include <vector>

using std::cout;
using std::endl;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}


bool Chess::test_move( std::pair< char , char > start , std::pair< char , char > end ) const
{
  //check that end loc and start loc are valid
  if (!Board::onBoard(end)) {
    return false;
  }

  //test that piece is on start
  if(!_board(start)) {
    return false;
  }
  
  const Piece* startPiece = _board(start);
  const Piece* endPiece = _board(end);

  //check if start loc is valid (will return false if invalid loc or no piece on it)
  if (!startPiece) {
    return false;
  }

  //check if trying to move to starting spot
  if (start == end) {
    return false;
  }
  
  //if there's a piece on end:
  if (endPiece != NULL) {
    //check that start and end piece are different colors
    if (startPiece->is_white() == endPiece->is_white()) {
      return false;
    }
    //check if legal capture shape
    if (!startPiece->legal_capture_shape(start, end)) {
      return false;
    }
  }

  //if end is empty:
  else {
  //check if legal move shape
    if (!startPiece->legal_move_shape(start, end)) {
    return false;
    }
  }
  
  //check if the path is clear if the path is a straight line
  if (!clear_path(start, end)) {
    return false;
  }
  
  //if it passes all tests:
 return true;
}

bool Chess::make_move (std::pair< char , char > start , std::pair< char , char > end) {

  //check that start is on board - need this in test and make move
  if (!_board(start)) {
      return false;
    }

  //check that right color piece is moving
  if (_board(start)->is_white() != turn_white()) {
    return false;
  }

  //remove piece from start and end (if there is one on end) and add the moving piece to the end
  if (test_move(start, end)) {
    //check if the move puts the starting piece color in check
    Chess new_board = *this; //deep copy the entire game
    new_board._board.remove_piece(end);
    new_board._board.add_piece(end, new_board._board(start)->to_ascii());
    new_board._board.remove_piece(start);
    if(new_board.in_check(turn_white())) {
    return false;                                                                                                                                           
    }

    //move the piece
      _board.remove_piece(end);
      _board.add_piece(end, _board(start)->to_ascii());
      _board.remove_piece(start);

      //pawn promotion
      if ((end.second == '8' && _board(end)->to_ascii() == 'P')
	  || (end.second == '1' && _board(end)->to_ascii() == 'p')) {
	char queen = _board(end)->to_ascii() + 1; // because 'p' + 1 = 'q'
	_board.remove_piece(end);
	_board.add_piece(end, queen);
      }

      //switch the turn
      _turn_white = !_turn_white;

      return true;
  }
  return false;
}

bool Chess::clear_path (std::pair <char, char > start , std::pair< char , char > end) const {

  int length = 0; //length of path
  int it_x = 0; //incrementing across columns
  int it_y = 0; //incrementing across rows
  
  //if path the is diagonal: slope will be + or - 1
  if (start.first - end.first == start.second - end.second
      || start.first - end.first == end.second - start.second) {
    it_x = 1;//1 indicated a positive slope
    it_y = 1;
    length = end.first - start.first;//length of move
    if (start.first > end.first) {
      it_y = -1;
      length = -1 * length;//based on slope
    }
    if (start.second > end.second) {//direction is down
      it_x = -1;
    }
  }
  //if the path is vertical: check across columns
  else if (start.first == end.first) {
    if (end.second > start.second) {//direction is up
    it_x = 1;
    length = end.second - start.second;
    }
    else {//direction down
      it_x = -1;
      length = start.second - end.second;
    }
  }
  //if the path is horizontal: check across rows
  else if (start.second == end.second) {
    if (end.first > start.first) {//direction right
    it_y = 1;
    length = end.first - start.first;
    }
    else {//direction left
      it_y = -1;
      length = start.first - end.first;
    }
  }
  //if the path is...mysterious ;)
  else {
    return true;
  }
  std::pair <char, char> spot;
  spot.first = start.first;
  spot.second = start.second;
  //check if there's a piece along the path
  for (int i = 0; i < length - 1; i++) {
    spot.first = spot.first + it_y;
    spot.second = spot.second + it_x;
    if (_board(spot)) {//piece on path
      return false;
    }
  }
  return true;
}

//loops thru the board to find location of white/black king, depending on argument
std::pair< char, char> Chess::find_king ( bool white ) const {

  //define if we are looking for a white or black king
  char king = 'k';
  if (white) {
    king = 'K';
  }
  std::pair<char, char> spot;

  //loop through the board to find the location of var king
  for (char r = '1'; r <= '8'; r++) {
    for (char c = 'A'; c <= 'H'; c++) {
      spot = std::make_pair(c, r);
      const Piece* piece = _board(spot);
      if (piece != nullptr && piece->to_ascii() == king) {
	return spot;
      }
    }
  }
  
  //this essentially returns NULL because this pair is not on the board
  return std::make_pair('a','a');
}

//check if game is in check
bool Chess::in_check( bool white ) const
{
  std::pair <char, char> king_loc = find_king(white); //find location of king
  //check if there's a spot on the board with a piece that can capture the king
  std::pair <char, char> spot;
  for (char r = '1'; r <= '8'; r++) {
    for (char c = 'A'; c <= 'H'; c++) {
      spot = std::make_pair(c, r);
      if (test_move(spot, king_loc)) { //test if any loc on board can capture the king
	return true;
      }
    }
  }
  return false;
}

//check if game is in checkmate
bool Chess::in_mate( bool white ) const
{
  //must be in check to be in checkmate
  if (!in_check(white)) {
    return false;
  }

  
  //make list of every possible location on board
  std::vector< std::pair<char, char> > possibleBoardLocations;
  
  for (char r = '1'; r <= '8'; r++) {
    for (char c = 'A'; c <= 'H'; c++) {
      std::pair <char, char> currSpot =	std::make_pair(c, r);
      possibleBoardLocations.push_back(currSpot);
    }
  }

  //we will check if each piece (of the curr players color)
  //on the board can move to a location
  //that ends the check
  //for every piece on the board
  for (unsigned int i = 0; i < possibleBoardLocations.size(); i++) {
    std::pair<char, char> currPieceLoc = possibleBoardLocations[(int) i];
    const Piece* currPiece= _board( currPieceLoc );

    if (currPiece == nullptr) {
      //this is an empty space on the board
      continue;
    }
    if (currPiece->is_white() != white) {
      //we can't move this piece color
      continue;
    }

    //for every possible location on the board
    for (unsigned int k = 0; k < possibleBoardLocations.size(); k++) {
      Chess newBoard = *this; //deep copy the entire game
      std::pair<char, char> testPieceLoc = possibleBoardLocations[(int) k];

      //see if moving our piece to this location ends the check
      if (newBoard.make_move(currPieceLoc, testPieceLoc)
	  && !newBoard.in_check(white)){
	return false;
      }
    }
  }

  //None of our above checks found a method for getting the king out of check
  //Therefore the game is checkmate
  return true;
}

bool Chess::in_stalemate( bool white ) const
{
  //if we were in check then it's not a stalemate
  //by the definition of stalemate
  if (in_check(white)) {
    return false;
  }
  
  //make list of every possible location on board
  std::vector< std::pair<char, char> > possibleBoardLocations;
  for (char r = '1'; r <= '8'; r++) {
    for (char c = 'A'; c <= 'H'; c++) {
      std::pair <char, char> currSpot = std::make_pair(c, r);
      possibleBoardLocations.push_back(currSpot);
    }
  }
  
  //we will check if each piece (of the current players color)
  //on the board can legally move to any position
  //Thus, proving that we are not in stalemate
  
  //for every piece on the board
  for (unsigned int i = 0; i < possibleBoardLocations.size(); i++) {
    std::pair<char, char> currPieceLoc = possibleBoardLocations[(int) i];
    const Piece* currPiece = _board( currPieceLoc );
    
    if (currPiece == nullptr) {
      //this is an empty space on the board
      continue;
    }
    
    if (currPiece->is_white() != white) {
      //we can't move this piece color
      continue;
    }
    
    //for every possible location on the board
    for (unsigned int k = 0; k < possibleBoardLocations.size(); k++) {
      std::pair<char, char> testPieceLoc = possibleBoardLocations[(int) k];
      
      //see if moving our piece to this location is legal
      if (test_move(currPieceLoc, testPieceLoc)) {
	//check if the move puts the starting piece color in check
	Chess new_board = *this; //deep copy the entire game
	new_board._board.remove_piece(testPieceLoc);
	new_board._board.add_piece(testPieceLoc, new_board._board(currPieceLoc)->to_ascii());
	new_board._board.remove_piece(currPieceLoc);
	if(new_board.in_check(turn_white())) {
	  continue;                                                                                                                                           
	}
	return false;
      }
    }
  }
  //None of our above checks found a legal move
  //Therefore the game is stalemate
  return true;
}

void Chess::empty_board() {
 
//make list of every possible location on board                                                                                                                                   
  std::vector< std::pair<char, char> > possibleBoardLocations;

  for (char r = '1'; r <= '8'; r++) {
    for (char c = 'A'; c <= 'H'; c++) {
      std::pair <char, char> currSpot = std::make_pair(c, r);
      possibleBoardLocations.push_back(currSpot);//add all board locations to the vector
    }
  }

  //loop through every possible location on chess board
  for (unsigned int i = 0; i < possibleBoardLocations.size(); i++) {
    _board.remove_piece( possibleBoardLocations[i] );
  }
  return;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

std::istream& operator >> ( std::istream& is , Chess& chess )
{
  //this function is friend of Chess
  chess.empty_board();

  //loop through every character in the parameter, is
  char boardLoc;
  
  for (int i = 0; i < 64; i++) {
    is >> boardLoc;
    if (boardLoc != '-') {
      const int widthOfBoard = 8;
      //calculate the row and column based on the i
      const char letter = 'A' + i % widthOfBoard;
      const char number = '8' - i / widthOfBoard;
      
      std::pair<char, char> loc = std::make_pair(letter, number);
      chess._board.add_piece(loc, boardLoc);//add piece
    }
  }

  //Check whose turn it is
  is >> boardLoc;
  
  switch (boardLoc) {//change turn based on current piece
    case 'b': case 'B':
      chess.change_turn(false);
      break;
    case 'w': case 'W':
      chess.change_turn(true);
  }
  
  return is;
}
